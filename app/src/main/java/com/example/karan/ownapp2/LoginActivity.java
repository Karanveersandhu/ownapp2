package com.example.karan.ownapp2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Login Activity";
    private TextView status;
    private TextView detail;
    private EditText etEmail;
    private EditText etPassword;

    private FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        status = (TextView) findViewById(R.id.status);
        detail = (TextView) findViewById(R.id.detail);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        fAuth = FirebaseAuth.getInstance();

        findViewById(R.id.bSignIn).setOnClickListener(this);
        findViewById(R.id.bCreateAcc).setOnClickListener(this);
        findViewById(R.id.bSignOut).setOnClickListener(this);
        findViewById(R.id.bTestStorage).setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = fAuth.getCurrentUser();
        updateUI(user);
    }

    private void updateUI(FirebaseUser user){
        if (user!=null){
            status.setText("User Email" + user.getEmail());
            detail.setText("Firebase UID"  + user.getUid());

            findViewById(R.id.email_password_buttons).setVisibility(View.GONE);
            findViewById(R.id.email_password_fields).setVisibility(View.GONE);
            findViewById(R.id.layout_signed_in_control).setVisibility(View.VISIBLE);
        }else {
            status.setText("Signed Out");
            detail.setText(null);

            findViewById(R.id.email_password_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.email_password_fields).setVisibility(View.VISIBLE);
            findViewById(R.id.layout_signed_in_control).setVisibility(View.GONE);
        }

    }

    private void CreateAccount(String email,String password){
        Log.e(TAG, "createAccount:" + email);
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Creating...");
        if (!validate(email,password)) {
            return;
        }
        fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    Log.e(TAG,"Create Account : Success");
                    Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();

                    FirebaseUser user = fAuth.getCurrentUser();
                    updateUI(user);
                }else {
                    Log.e(TAG, "Create account : Failed", task.getException());
                    Toast.makeText(LoginActivity.this, "Error while Creating", Toast.LENGTH_SHORT).show();
                    updateUI(null);
                }
            }
        });
    }

    private void SignIn(String email,String password){
        Log.e(TAG,"sign in" + email);
        if (!validate(email, password)){
            return;
        }
        fAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    Log.e(TAG,"Sign In : Success");
                    Toast.makeText(LoginActivity.this, "Signed in", Toast.LENGTH_SHORT).show();
                    FirebaseUser user = fAuth.getCurrentUser();
                    updateUI(user);
                }else {
                    Log.e(TAG,"Sign in : Error" + task.getException());
                    updateUI(null);
                    Toast.makeText(LoginActivity.this, "Not signed In", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void SignOut(){
        fAuth.signOut();
        updateUI(null);
    }

    private boolean validate(String email,String password){
        if (TextUtils.isEmpty(email)){
            Toast.makeText(this, "please enter email address", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(password)){
            Toast.makeText(this, "please enter a password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.length() < 6){
            Toast.makeText(this, "The password is too small", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.bCreateAcc){
            CreateAccount(etEmail.getText().toString(),etPassword.getText().toString());
        }else if (i == R.id.bSignIn){
            SignIn(etEmail.getText().toString(),etPassword.getText().toString());
        }else if (i == R.id.bSignOut){
            SignOut();
        }else if ( i == R.id.bTestStorage){
            Storage();
        }
    }

    private void Storage(){
        startActivity(new Intent(this,MainActivity.class));

    }

}

