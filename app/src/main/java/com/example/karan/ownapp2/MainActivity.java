package com.example.karan.ownapp2;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Main Activity";

    private static final int CHOOSING_IMAGE_REQUEST = 1234;

    private Uri fileUri;
    private DatabaseReference myRef;
    private StorageReference folderRef;
    private StorageReference fileRef;
    private EditText etName;
    private EditText etPrice;
    private RecyclerView recyclerView;
    private FirebaseRecyclerAdapter<UploadInfo,ImageViewHolder> firebaseRecyclerAdapter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = (EditText) findViewById(R.id.etName);
        etPrice = (EditText) findViewById(R.id.etPrice);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        myRef = FirebaseDatabase.getInstance().getReference("Images");
        folderRef = FirebaseStorage.getInstance().getReference().child("Images");
        fileRef = null;

        progressDialog = new ProgressDialog(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);

        findViewById(R.id.bChooseFile).setOnClickListener(this);
        findViewById(R.id.bUploadFile).setOnClickListener(this);
        findViewById(R.id.bBack).setOnClickListener(this);

        Query query = myRef.limitToLast(8);

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<UploadInfo, ImageViewHolder>(UploadInfo.class,R.layout.item_layout,ImageViewHolder.class,query) {
            @Override
            protected void populateViewHolder(ImageViewHolder viewHolder, UploadInfo model, int position) {

                viewHolder.tvName.setText(model.name);
                viewHolder.tvPrice.setText(model.price);
                Picasso.with(MainActivity.this).load(model.url).error(R.drawable.common_full_open_on_phone).into(viewHolder.imageView);
            }
        };

        recyclerView.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    public void onClick(View view) {

        int i = view.getId();
        if(i == R.id.bChooseFile){
            showChoosingFile();
        }else if (i == R.id.bUploadFile){
            UploadFile();
        }else if (i == R.id.bBack){
            finish();
        }
    }

    private void UploadFile(){
        if (fileUri != null){
            String name = etName.getText().toString();
            String price = etPrice.getText().toString();

            if (!Validate(name)){
                return;
            }

            progressDialog.setMessage("Uploading...");
            progressDialog.show();


            fileRef = folderRef.child(name + "." +  getFileExtension(fileUri));
            fileRef.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    progressDialog.dismiss();
                    String name = taskSnapshot.getMetadata().getName();
                    String url = taskSnapshot.getDownloadUrl().toString();
                    String price = taskSnapshot.getMetadata().getName();

                    Log.e(TAG, "Uri: " + url);
                    Log.e(TAG, "Name: " + name);
                    Log.e(TAG, "Price" + price);

                    writeNewImageInfoToDB(name,price,url);

                    Toast.makeText(MainActivity.this, "File Uploaded", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void writeNewImageInfoToDB(String name,String price,String url){
        UploadInfo info = new UploadInfo(name,price,url);

        String key = myRef.push().getKey();
        myRef.child(key).setValue(info);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        firebaseRecyclerAdapter.cleanup();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == CHOOSING_IMAGE_REQUEST && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            fileUri = intent.getData();
        }
    }


    private void showChoosingFile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), CHOOSING_IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();

        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private boolean Validate(String fileName){
        if (TextUtils.isEmpty(fileName)){
            Toast.makeText(this, "Please fill the remaining fields", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
