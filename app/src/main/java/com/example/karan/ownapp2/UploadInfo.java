package com.example.karan.ownapp2;

/**
 * Created by karan on 4/2/2018.
 */
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class UploadInfo {

    public String name;
    public String price;
    public String url;

    public UploadInfo() {
    }

    public UploadInfo(String name,String price,String url) {
        this.name = name;
        this.price = price;
        this.url= url;
    }
}