package com.example.karan.ownapp2;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by karan on 4/2/2018.
 */


public class ImageViewHolder extends RecyclerView.ViewHolder{

    public TextView tvName;
    public TextView tvPrice;
    public ImageView imageView;

    public ImageViewHolder(View itemView){
        super(itemView);

        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);

    }

}
